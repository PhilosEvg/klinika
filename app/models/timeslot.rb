require 'date'

class Timeslot < ApplicationRecord
  belongs_to :doctor
  belongs_to :appointment, optional: true

  validates_uniqueness_of :app_time, scope: %i[app_date doctor_id]
  validate :lunch?, :weekend?
  validates_time :app_time, between: ['9:00', '17:00']
  validates_date :app_date, after: Date.today
  # def to_param
  #   app_date
  # end

  private

  def lunch?
    if app_time.strftime('%H') == '13'
      errors.add(:app_time, "it's time for doctor's lunch")
    end
  end

  def weekend?
    if app_date.strftime('%u') == '6' || app_date.strftime('%u') == '7'
      errors.add(:app_date, "The clinics doesn't work at weekend")
    end
  end
end
