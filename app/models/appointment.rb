class Appointment < ApplicationRecord
  belongs_to :doctor
  belongs_to :patient
  has_one :timeslot, validate: false

  accepts_nested_attributes_for :patient

  validates :doctor, :patient, :timeslot, presence: true
end
