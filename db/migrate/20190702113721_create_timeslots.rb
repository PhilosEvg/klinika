class CreateTimeslots < ActiveRecord::Migration[5.2]
  def change
    create_table :timeslots do |t|
      t.date :app_date
      t.time :app_time
      t.boolean :status
      t.references :doctor, foreign_key: true
      t.references :appointment, foreign_key: true

      t.timestamps
    end
  end
end
