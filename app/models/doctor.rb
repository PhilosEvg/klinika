class Doctor < ApplicationRecord
  has_many :patients, through: :appointments
  has_many :appointments
  has_many :timeslots, validate: false
end
