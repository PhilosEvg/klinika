class DoctorsController < ApplicationController
  before_action :set_doctor, only: [:show, :edit, :update, :destroy]

  # GET /doctors
  # GET /doctors.json
  def index
    @doctors = Doctor.all
  end

  # GET /doctors/1
  # GET /doctors/1.json
  def show
    @timeslots = Timeslot.where(doctor_id: @doctor.id)
    @timeslots = @timeslots.select { |t| t.status == nil }
    # @same_dates = []
    # @timeslots.each { |t| @same_dates << t.app_date }
    # @same_dates = @same_dates.uniq
    @appointment = @doctor.appointments.build#(doctor_id: @doctor.id, appointment_date: )
    # while @same_dates.app_date.uniq do
    #   @timeslots.each { |t| same_dates << t.app_date}
    # end

    # binding.pry
  end

  # GET /doctors/new
  def new
    @doctor = Doctor.new
  end

  # GET /doctors/1/edit
  def edit
  end

  # POST /doctors
  # POST /doctors.json
  def create
    @doctor = Doctor.new(doctor_params)

    # respond_to do |format|
    #   if @doctor.save
    #     format.html { redirect_to @doctor, notice: 'Doctor was successfully created.' }
    #     format.json { render :show, status: :created, location: @doctor }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @doctor.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /doctors/1
  # PATCH/PUT /doctors/1.json

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor
      @doctor = Doctor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_params
      params.fetch(:doctor, {})
    end
end
