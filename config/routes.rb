Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'doctors#index'

  # resources :doctors, only: [:create]
  resources :doctors, only: [:index, :show, :new, :create] do
    resources :timeslots, only: [:index, :new, :create, :show] do
      resources :appointments, only: [:new, :create, :index, :show]
    end
  end
  resources :patient, only: [:new, :create]
end
