# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Doctor.create(specialization: 'Терапевт')
Doctor.create(specialization: 'Иммунолог')
Doctor.create(specialization: 'Отоларинголог')
Timeslot.create(app_date: '04.07.2019', app_time: '9:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '04.07.2019', app_time: '10:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '04.07.2019', app_time: '11:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '04.07.2019', app_time: '12:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '04.07.2019', app_time: '14:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '04.07.2019', app_time: '15:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '04.07.2019', app_time: '16:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '04.07.2019', app_time: '17:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '09:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '10:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '11:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '12:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '14:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '15:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '16:00', doctor_id: Doctor.first.id, status: false)
Timeslot.create(app_date: '05.07.2019', app_time: '17:00', doctor_id: Doctor.first.id, status: false)

