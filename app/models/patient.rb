class Patient < ApplicationRecord
  has_many :appointments
  has_many :doctors, through: :appointments
  has_many :timeslots
  validates :name, :surname, :birthdate, presence: true
  validates_uniqueness_of :name, scope: %i[surname birthdate]
end
