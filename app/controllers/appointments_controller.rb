class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  def show
  end

  # GET /appointments/new
  def new
    # binding.pry
    @doctor = Doctor.find(params[:doctor_id])
    # @doctor = Doctor.find_by(id: params[:doctor_id])
    @timeslot = @doctor.timeslots.find(params[:timeslot_id])
    # @appointment = Appointment.new(doctor_id: @doctor.id, timeslot: @timeslot)
    @appointment = @doctor.appointments.new
    # @appointment = Appointment.find(params[:id])
    @appointment.build_patient
    # binding.pry
  end

  # GET /appointments/1/edit
  def edit
  end

  # POST /appointments
  # POST /appointments.json
  def create
    # binding.pry
    @doctor = Doctor.find(params[:doctor_id])
    patient = Patient.where(
      name: params[:patient][:name],
      surname: params[:patient][:surname], birthdate: params[:patient][:birthdate]
      ).first_or_create
      # birthdate: "#{params[:patient][:"birthdate(1i)"]}.#{params[:patient][:"birthdate(2i)"]}.#{params[:patient][:"birthdate(3i)"]}"

    timeslot = Timeslot.find_by(id: params[:timeslot_id])
    appointment = Appointment.create(
      doctor_id: params[:doctor_id], timeslot: timeslot, patient_id: patient.id
      ) if (timeslot.app_date >= Date.today && timeslot.app_time > Time.now.hour)

    if appointment.save
      timeslot.update_attribute(:status, true)
      # appointment.timeslot.status = true
      redirect_to @doctor, notice: 'Appointment was successfully created'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    # def appointment_params
    #   params.require(:appointment).permit(:doctor_id, :timeslot_id, patient_attributes: [:name, :surname, :birthdate])#(:appointment_date, :doctor_id, :patient_id, :timeslot_id)
    # end
end
